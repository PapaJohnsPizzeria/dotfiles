#
# ~/.bashrc
#

exec fish

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

### ALIASES ###
alias ls='exa -al --header --icons --git'
alias clear='clear;source ~/.bashrc'

PS1='[\u@\h \W]\$ '

eval "$(starship init bash)"

# fm6000 -dog -de "Qtile" -n -c bright_magenta

aura

. "$HOME/.cargo/env"
