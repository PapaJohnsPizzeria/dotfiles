#!/usr/bin/env bash

## config directory ##
cp -r ~/.config/qtile/* ./.config/qtile
cp -r ~/.config/fish/* ./.config/fish
cp -r ~/.config/neofetch/* ./.config/neofetch
cp -r ~/.config/rofi/* ./.config/rofi
cp -r ~/.config/i3/* ./.config/i3
cp -r ~/.config/alacritty/* ./.config/alacritty
cp ~/.config/starship.toml ./.config/starship.toml
echo "finished cloning config directory..."

## home directory ##
cp ~/.bashrc ./.bashrc
cp ~/.xinitrc ./.xinitrc
cp ~/.vimrc ./.vimrc
cp ~/.xbindkeysrc ./.xbindkeysrc
echo "finished cloning home directory..."

echo "end of script"
