# lives in ~/.config/qtile/config.py

import os
import subprocess
from libqtile import hook

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()

keys = [
    # betterlockscreen
    Key([mod], "x", lazy.spawn("betterlockscreen -l blur"), desc="lock screen"),
    # spawn most used items (enter for terminal)
    Key([mod], "w", lazy.spawn("rofi -show run")), #rofi -show run
    Key([mod], "a", lazy.spawn("atom")),
    Key([mod], "s", lazy.spawn("rofi -show drun")), #rofi -show drun
    Key([mod], "d", lazy.spawn("chromium")),
    Key([mod], "f", lazy.spawn("pcmanfm")),
    Key([], "Print", lazy.spawn("flameshot gui"), desc="Screenshot"),
    Key([mod], "o", lazy.spawn("Obsidian")),
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"],"Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    # Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "Tab", lazy.window.toggle_floating(), desc="Toggle floating window"),

    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

groups = [
    Group(
        '1',
        label="DEV",
        layout='Columns',
        spawn="atom",
    ),
    Group('2', label="WEB", layout='Columns'),
    Group('3', label="WEB", layout='Columns'),
    Group(
        '4',
        label="ZOOM",
        layout='Columns',
        matches=[
            Match(wm_class=["zoom"]),
        ],
    ),
    Group(
        '5',
        label="CHAT",
        layout='Columns',
        spawn="discord",
        ),
    Group('6', label="VBOX", layout='floating'),
    Group('7', label="MUSIC", layout='Columns'),
    Group('8', label="RAND", layout='floating')
]

# Allow MODKEY+[0 through 9] to bind to groups,
# see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )


layout_theme = {
    "border_width": 2,
    "margin": 5,
    "border_focus": "b344bd",
    # "border_normal": "1D2330",
    }

layouts = [
    layout.Columns(**layout_theme),
    layout.Floating(**layout_theme),
    # layout.Max(**layout_theme),
    # layout.MonadTall(margin = 8),
]

colors = [
    ["#ec8280","#ec8280"], # 0
    ["#e6a09e","#e6a09e"], # 0
    ["#f381fc","#f381fc"], # 1
    ["#a1adf7","#a1adf7"], # 2
    ["#fbdad4","#fbdad4"], # 3
    ["#999999","#999999"], # 4
    ["#595959","#595959"], # 5
    ["#302025","#302025"], # 6
]

widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize=12,
    padding=5,
)
extension_defaults = widget_defaults.copy()

# def top_bar():


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    padding=6,
                    linewidth=0,
                    background=colors[4],
                ),
                widget.Image(
                    filename='~/.face',
                    # mouse_callbacks={
                    #     "Button1": lambda: shutdown()")
                    # },
                    background=colors[4],
                ),
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[7],
                    background=colors[4],
                ),
                widget.GroupBox(
                    font = "Ubuntu Bold",
                    rounded = False,
                    highlight_method = "text",
                    this_current_screen_border = colors[4],
                    active = colors[5],
                    inactive = colors[6],
                ),
                ### WindowName
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[4],
                    background=colors[7],
                ),
                widget.WindowName(
                    foreground=colors[7],
                    background=colors[4],
                ),
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[7],
                    background=colors[4],
                ),
                ### Gap
                widget.Spacer(
                    length=550,
                ),
                ### Battery
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[3],
                    background=colors[7],
                ),
                widget.TextBox(
                    text='',
                    fontsize="35",
                    foreground=colors[7],
                    background=colors[3],
                ),
                widget.Battery(
                    format='{percent:2.0%}',
                    charge_char='^',
                    show_short_text=False,
                    foreground=colors[7],
                    background=colors[3],
                ),
                ### Volume
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[2],
                    background=colors[3],
                ),
                widget.TextBox(
                    text='墳',
                    fontsize="30",
                    foreground=colors[7],
                    background=colors[2],
                ),
                widget.PulseVolume(
                    foreground=colors[7],
                    background=colors[2],
                ),
                ### CheckUpdates
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[1],
                    background=colors[2],
                ),
                widget.TextBox(
                    text='',
                    fontsize="30",
                    # foreground=colors[7],
                    background=colors[1],
                ),
                widget.CheckUpdates(
                    no_update_string='No updates',
                    # foreground=colors[0],
                    background=colors[1],
                ),
                ### Clock
                widget.TextBox(
                    text="\ue0be",
                    fontsize="32",
                    padding=0,
                    foreground=colors[0],
                    background=colors[1],
                ),
                widget.TextBox(
                    text='',
                    fontsize="30",
                    # foreground=colors[4],
                    background=colors[0],
                ),
                widget.Clock(
                    format="%b %d - %I:%M:%S %p",
                    background=colors[0],
                ),
                ### Systray
                widget.Systray(
                    background=colors[0],
                ),
                widget.Sep(
                    foreground=colors[0],
                    background=colors[0],
                ),
            ],
            size=24,
            margin=5,
            background=colors[7],
            opacity=1,
	    # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
	    # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/boot.sh"])
