#!/usr/bin/env bash

picom --experimental-backends --fade-delta=2 -b &
xbindkeys &
nm-applet &
flameshot &
nitrogen --restore &
