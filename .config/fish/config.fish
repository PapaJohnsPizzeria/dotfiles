# ~/.config/fish/config.fish

### Remove fish greeting ###
set fish_greeting

# Commands to run in interactive sessions can go here
if status is-interactive
end

### ALIASES ###
alias ls="exa -al --header --icons --git"
alias tree="exa -T"

# alias clear="clear; aura"
alias c="clear; source ~/.config/fish/config.fish"
alias cls="c;ls"
alias clls="clear;ls"

# thefuck --alias | source
# alias todo="vim ~/todo.md"

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias cat="bat"

alias apm="apm --color"
alias fucklinter="rm *.gch"

### "bat" as manpager ###
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

### STARSHIP ###
starship init fish | source

### AuraFetch (is broken kinda) ###
aura
